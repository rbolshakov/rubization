//
//  RubyService.swift
//  Rubization
//
//  Created by Roman Bolshakov on 2019/07/28.
//  Copyright © 2019 Roman Bolshakov. All rights reserved.
//

import Foundation

let app_id = "03617cc7257b7eda3b336a3cac36dfc506ecb57d976c4b42a3fd9ccd53dc88dd"
let service_url = "https://labs.goo.ne.jp/api/hiragana"

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

class RubyService {
    func convertKanji(kanji: String, completion: @escaping (_ result: String)->(), failure: @escaping (_ error: String)->())  {
        let url = URL(string: service_url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let parameters: [String: String] = [
            "app_id": app_id,
            "sentence": kanji,
            //Consider creating parameter
            "output_type": "hiragana"
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                failure(error.localizedDescription)
                return
            }
            if response == nil {
                failure("Could not get responce from server")
                return
            }
            guard let data = data else {
                failure("Could not get data from server")
                return
            }
            guard let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String:AnyObject] else {
                failure("Server responded with wrong output")
                return
            }
            guard let converted = json["converted"] as? String else {
                failure("Server responded with wrong output")
                return
            }
            //print (converted)
            completion(converted)
        }
        task.resume()
    }
}
