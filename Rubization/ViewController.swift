//
//  ViewController.swift
//  Rubization
//
//  Created by Roman Bolshakov on 2019/07/28.
//  Copyright © 2019 Roman Bolshakov. All rights reserved.
//

import UIKit

let service = RubyService()
class ViewController: UIViewController {
    
    var keyboardShown = false
    @IBOutlet weak var inTextView: UITextView!
    @IBOutlet weak var outTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareInterface()
        
    }
    @IBAction func convertButtonPressed(_ sender: Any) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.outTextView.text = ""
        self.inTextView.resignFirstResponder()
        service.convertKanji(kanji: inTextView.text, completion: { (converted) in
            DispatchQueue.main.async {
                self.outputText(text: converted)
            }
        }) { (failure) in
            DispatchQueue.main.async {
                self.outputText(text: failure)
            }
        }
    }
    
    func prepareInterface () {
        self.activityIndicator.isHidden = true
        
        inTextView.layer.cornerRadius = 5
        inTextView.layer.masksToBounds = true
        inTextView.layer.borderColor = UIColor.gray.cgColor
        inTextView.layer.borderWidth = 0.9
    }
    
    func outputText(text: String) {
        self.outTextView.text = text
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
}

